# PostgreSQL
The repository contains manifests for the persistent volume, claim, pod, service in the
Kubernetes cluster and CI/CD pipeline script. Requires availability
GitLab DB_PASSWORD variable containing the database password.